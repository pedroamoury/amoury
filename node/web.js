var http = require('http');
var server = http.createServer(function(request, response){
	response.writeHead(200, {"Content-Type": "text/html; charset=utf-8"});
	
	response.write('<div style="color:red; align:center;">');
	
	if(request.url == "/"){
		response.write('<h1> Pagina Inicial </h1>');
		response.write('<input type="button" value="Teste">');
	}else if(request.url == '/rota01'){
		response.write('<h1> Bem-vindo !');		
	}else{
		response.write("<h1> Pagina não encontrada</h1>");
	}
	
	response.write('<div>');
	response.end();
});

server.listen(3000, function(){
	console.log('Servidor Rodando...');
});